import { INode, INodeExecutionData } from "../../utils/interfaces";

export class StartNode implements INode {
  NodeDiscription: any = {
    displayName: "Start",
    version: 1,
    description: "This is a descrption",
    inputs: [],
    outputs: ["main"],
    properties: [],
  };

  async execute(
    outputPrevNode: INodeExecutionData
  ): Promise<INodeExecutionData> {
    const data: INodeExecutionData = {
      executedNode: this.NodeDiscription.displayName,
      prevNodeData: [],
      json: {},
      endpointParameters: outputPrevNode.json,
    };
    return Promise.resolve(data);
  }
}
