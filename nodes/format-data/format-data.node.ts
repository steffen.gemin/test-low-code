import {
  GenericValue,
  IDataObject,
  INode,
  INodeExecutionData,
} from "../../utils/interfaces";

export class FormatNode implements INode {
  parameters: IDataObject = {};

  constructor(par?: IDataObject) {
    let approvedParameters: Array<string> = this.NodeDiscription.properties.map(
      (k: any) => k.name
    );

    if (par !== undefined) {
      for (const [key, value] of Object.entries(par)) {
        if (approvedParameters.includes(key)) {
          this.parameters = {
            ...this.parameters,
            [key]: value,
          };
        }
      }
    }
  }

  getParam(key: string): GenericValue | GenericValue[] {
    if (this.parameters.hasOwnProperty(key)) {
      return this.parameters[key];
    }
    return undefined;
  }

  NodeDiscription: any = {
    displayName: "Ping",
    version: 1,
    description: "This is a descrption",
    inputs: [],
    outputs: ["main"],
    properties: [
      {
        name: "format",
        displayName: "json format",
        type: "object",
        default: "",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
    ],
  };

  async execute(
    outputPrevNode: INodeExecutionData
  ): Promise<INodeExecutionData> {
    const format: object = this.getParam("format") as object; //Temp parse to object should be handeld in get param

    // let newRes: any;

    // const prevOutput = outputPrevNode.json as unknown as Array<IDataObject>;
    // console.log(prevOutput);
    // let reworkedObj;

    // prevOutput.forEach((item) => {
    //   for (const [key, value] of Object.entries(format)) {
    //     const v = findValue(item, value);
    //     console.log({
    //       [key]: v,
    //     });
    //   }
    // });

    //Start altijd met een object met daarin keys: value (value kan eventueel array zijn)
    // {"json":{"results":[{"_id":"61ffc6c156a37c4c51baef72","user":"steffen"}]}}
    // Stel enkel results naar resultaaten renamen input {resultaten: results} => output json:{resultaten: []}
    // Stel enkel eerste user weergeven met key superuser {superuser: results.0.user} => output json: {superuser: steffen}

    const data = outputPrevNode;
    return data;
  }
}

function findValue(obj: IDataObject, prop: string) {
  const fields = prop.split(".");
  let newObj: GenericValue[] | GenericValue;
  for (var i = 0; i < fields.length; i++) {
    if (typeof obj[fields[i]] == "undefined") return undefined;
    newObj = obj[fields[i]];
    console.log(obj);
  }
  return newObj;
}
