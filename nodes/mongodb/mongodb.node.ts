import {
  GenericValue,
  IDataObject,
  INode,
  INodeExecutionData,
} from "../../utils/interfaces";

import mongoose, { Model, model, PromiseProvider, Schema } from "mongoose";

export class MongoDb implements INode {
  parameters: IDataObject = {};

  constructor(par?: IDataObject) {
    let approvedParameters: Array<string> = this.NodeDiscription.properties.map(
      (k: any) => k.name
    );

    if (par !== undefined) {
      for (const [key, value] of Object.entries(par)) {
        if (approvedParameters.includes(key)) {
          this.parameters = {
            ...this.parameters,
            [key]: value,
          };
        }
      }
    }
  }

  getParam(key: string): GenericValue | GenericValue[] {
    if (this.parameters.hasOwnProperty(key)) {
      return this.parameters[key];
    } else {
      const temp = this.NodeDiscription.properties.find(
        (p: any) => p.name === key
      );
      return temp.default;
    }
  }

  NodeDiscription: any = {
    displayName: "MongoDB",
    version: 1,
    description: "This is a descrption",
    inputs: [],
    outputs: ["main"],
    properties: [
      {
        name: "collection",
        displayName: "What collection",
        type: "string",
        default: "User",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "mongoUser",
        displayName: "What collection",
        type: "string",
        default: "mongouser",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "mongoUserPassword",
        displayName: "What collection",
        type: "string",
        default: "mongouser",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "cluster",
        displayName: "What collection",
        type: "string",
        default: "cluster0",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "databaseName",
        displayName: "What collection",
        type: "string",
        default: "collection1",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "action",
        displayName: "Who is pinging",
        type: "string",
        default: "",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "query",
        displayName: "Query",
        type: "string",
        default: "{}",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
      {
        name: "select",
        displayName: "select query",
        type: "string",
        default: "{}",
        placeholder: "Placeholder value",
        description: "This is a description",
      },
    ],
  };

  async execute(
    outputPrevNode: INodeExecutionData
  ): Promise<INodeExecutionData> {
    const mongoUser = this.getParam("mongoUser");
    const mongoUserPassword = this.getParam("mongoUserPassword");
    const cluster = this.getParam("cluster");
    const databaseName = this.getParam("databaseName");
    const collection = this.getParam("collection");

    const action = this.getParam("action") as string;

    const supportedActions = ["get", "post"];

    if (!supportedActions.includes(action)) {
      throw new Error("Action is not supported");
    }

    const url = `mongodb+srv://${mongoUser}:${mongoUserPassword}@${cluster}.xwroy.mongodb.net/${databaseName}?retryWrites=true&w=majority`;

    if (collection === undefined || collection === null || collection === "") {
      throw new Error("Collection has to be defined");
    }

    try {
      await mongoose.connect(url);
    } catch (e) {
      throw new Error("Failed to connect to mongoDB");
    }

    const Model = getModel(collection.toString());

    if (action === "get") {
      const query = (this.getParam("query") as object) || {};
      const select = (this.getParam("select") as object) || {};
      const newRes = await Model.find(query).select(select);
      //Format output
      const data: INodeExecutionData = {
        endpointParameters: outputPrevNode.endpointParameters,
        executedNode: this.NodeDiscription.displayName,
        prevNodeData: [
          ...outputPrevNode.prevNodeData,
          {
            name: outputPrevNode.executedNode,
            json: outputPrevNode.json,
          },
        ],
        json: {
          results: newRes,
        },
      };
      return Promise.resolve(data);
    }

    if (action === "post") {
      console.log(outputPrevNode);
      const newItem = new Model({
        _id: new mongoose.Types.ObjectId(),
        testValue: "dit is een test",
        ...outputPrevNode.endpointParameters.body,
      });
      const newRes = await newItem.save();

      const data: INodeExecutionData = {
        endpointParameters: outputPrevNode.endpointParameters,
        executedNode: this.NodeDiscription.displayName,
        prevNodeData: [
          ...outputPrevNode.prevNodeData,
          {
            name: outputPrevNode.executedNode,
            json: outputPrevNode.json,
          },
        ],
        json: {
          created: newRes,
        },
      };
      return Promise.resolve(data);
    }

    throw new Error("Something went wrong");
  }
}

function getModel(collection: string): Model<IDataObject> {
  const schema = new Schema<IDataObject>(
    {}, //TODO add schemas to allow only the data we want and disable strict
    { collection: collection.toString(), strict: false }
  );

  return mongoose.models.Model || model<IDataObject>("Model", schema);
}
