import {
  GenericValue,
  IDataObject,
  INode,
  INodeExecutionData,
} from "../../utils/interfaces";

export class LogNode implements INode {
  parameters: IDataObject = {};

  constructor(par?: IDataObject) {
    let approvedParameters: Array<string> = this.NodeDiscription.properties.map(
      (k: any) => k.name
    );

    if (par !== undefined) {
      for (const [key, value] of Object.entries(par)) {
        if (approvedParameters.includes(key)) {
          this.parameters = {
            ...this.parameters,
            [key]: value,
          };
        }
      }
    }
  }

  getParam(key: string): GenericValue | GenericValue[] {
    if (this.parameters.hasOwnProperty(key)) {
      return this.parameters[key];
    }
    return undefined;
  }

  NodeDiscription: any = {
    displayName: "Ping",
    version: 1,
    description: "This is a descrption",
    inputs: [],
    outputs: ["main"],
    properties: [],
  };

  async execute(
    outputPrevNode: INodeExecutionData
  ): Promise<INodeExecutionData> {
    const data = outputPrevNode;
    console.log(JSON.stringify(data));
    return data;
  }
}
