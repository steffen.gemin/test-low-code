import { FormatNode } from "./format-data/format-data.node";
import { LogNode } from "./log/log.node";
import { MongoDb } from "./mongodb/mongodb.node";
import { StartNode } from "./start/start.node";

export const nodes = new Map<String, any>([
  ["start", StartNode],
  ["mongodb", MongoDb],
  ["log", LogNode],
  ["format", FormatNode],
]);
