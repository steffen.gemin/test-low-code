import {
  GenericValue,
  IDataObject,
  INode,
  INodeExecutionData,
} from "../../utils/interfaces";

export class ReturnNode implements INode {
  parameters: IDataObject = {};

  constructor(par?: IDataObject) {
    let approvedParameters: Array<string> = this.NodeDiscription.properties.map(
      (k: any) => k.name
    );

    if (par !== undefined) {
      for (const [key, value] of Object.entries(par)) {
        if (approvedParameters.includes(key)) {
          this.parameters = {
            ...this.parameters,
            [key]: value,
          };
        }
      }
    }
  }

  getParam(key: string): GenericValue | GenericValue[] {
    if (this.parameters.hasOwnProperty(key)) {
      return this.parameters[key];
    }
    return undefined;
  }

  NodeDiscription: any = {
    displayName: "Return node",
    version: 1,
    description: "This is a descrption",
    inputs: [],
    outputs: ["main"],
    properties: [],
  };

  async execute(
    outputPrevNode: INodeExecutionData
  ): Promise<INodeExecutionData> {
    const data: INodeExecutionData = {
      endpointParameters: outputPrevNode.endpointParameters,
      executedNode: this.NodeDiscription.displayName,
      prevNodeData: [
        ...outputPrevNode.prevNodeData,
        {
          name: outputPrevNode.executedNode,
          json: outputPrevNode.json,
        },
      ],
      json: {
        ...outputPrevNode.json,
      },
    };

    return Promise.resolve(outputPrevNode);
  }
}
