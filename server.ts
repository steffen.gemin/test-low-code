import express from "express";
import { executeWorkflow } from "./core-functions";
import { endpoints } from "./flow";
import {
  INodeExecutionData,
  IRequestParameters,
  IResponse,
} from "./utils/interfaces";

const app = express();
const port = 8000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, () => {
  console.log("We are live on " + port);
});

app.get("/alive", (req, res) => {
  res.send("Yes sir");
});

if (endpoints !== undefined) {
  endpoints.forEach((e) => {
    app[e.verb](e.endpoint, (req: any, res: any) => {
      // TODO support query params
      // TODO support add validation for req body (allowed keys)

      const reqData: IRequestParameters = {
        urlParameters: req.params,
        body: req.body,
      };

      console.log(
        `ENPOINT ${e.endpoint}: ${e.verb} with data: ${JSON.stringify(reqData)}`
      );

      executeWorkflow(e.flow, reqData)
        .then((data: IResponse) => {
          res.send(data);
        })
        .catch((data: IResponse) => res.status(400).send(data));
    });
  });
}
