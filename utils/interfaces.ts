export type GenericValue =
  | string
  | object
  | number
  | boolean
  | undefined
  | null;

export interface IDataObject {
  [key: string]: GenericValue | IDataObject | GenericValue[] | IDataObject[];
}

export type NodeParameterValue = string | number | boolean | undefined | null;

interface prevNodedata {
  name: string;
  json?: IDataObject;
}

export interface INodeExecutionData {
  endpointParameters: IRequestParameters;
  executedNode: string;
  prevNodeData: Array<prevNodedata>;
  json: IDataObject;
  error?: String;
}

export interface IRequestParameters {
  urlParameters?: IDataObject;
  body?: IDataObject;
}

export interface INodeFunctions {
  getInputData(): INodeExecutionData[];
}

export interface IConnection {
  from: number;
  to: number;
  type: string;
}

export interface INodeStructure {
  id: Number;
  name: String;
  type: String;
  version: Number;
  parameters: object;
  postition: object;
}

export interface IWorkflowStructure {
  name: string;
  active: boolean;
  nodes: Array<INodeStructure>;
  connections: Array<IConnection>;
}

export interface INode {
  NodeDiscription: object;
  execute(outputPrevNode?: INodeExecutionData): Promise<INodeExecutionData>;
}

export interface INodeParameters {
  // TODO: Later also has to be possible to add multiple ones with the name name. So array has to be possible
  [key: string]:
    | NodeParameterValue
    | INodeParameters
    | NodeParameterValue[]
    | INodeParameters[];
}

export interface INodeType {
  NodeProperties: object;
  description: object;
  execute(this: any): Promise<INodeExecutionData>;
}

export interface INodeProperties {
  name: string;
  displayName: string;
  type: string;
  default: string;
  placeholder: NodeParameterValue | NodeParameterValue[];
  description: string;
}

export interface INodeTypeData {
  displayName: string;
  version: number;
  description: string;
  properties: INodeProperties[];
}

export interface IEndPoint {
  endpoint: string;
  verb: "get" | "post";
  flow: IWorkflowStructure;
}

export interface IResponse {
  json: IDataObject;
  error?: string;
}
