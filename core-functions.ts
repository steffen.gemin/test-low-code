import {
  IWorkflowStructure,
  INodeExecutionData,
  IConnection,
  INodeStructure,
  INode,
  IRequestParameters,
  IResponse,
} from "./utils/interfaces";
import { nodes } from "./nodes/nodes";

//Add input incase of req body / url parameters
export async function executeWorkflow(
  workflow: IWorkflowStructure,
  requestParameters: IRequestParameters
): Promise<IResponse> {
  const temp: INodeExecutionData = {
    endpointParameters: requestParameters,
    executedNode: "init",
    prevNodeData: [],
    json: {
      ...requestParameters,
    },
  };

  return new Promise((resolve, reject) => {
    executeNode(workflow, 1, temp)
      .then((e: INodeExecutionData) => {
        resolve({
          json: e.json,
        });
      })
      .catch((e: INodeExecutionData) => {
        reject({
          json: e.json,
          error: e.error,
        });
      });
  });
}

async function executeNode(
  workflow: IWorkflowStructure,
  from: Number = 1,
  outputPrevNode: INodeExecutionData
): Promise<INodeExecutionData> {
  return new Promise((resolve, reject) => {
    const nodeStructure: INodeStructure | undefined = workflow.nodes.find(
      (c) => c.id === from
    );

    if (nodeStructure === undefined) {
      throw new Error("Failed to get node structure");
    }
    const node: INode = new (nodes.get(nodeStructure.type))(
      nodeStructure.parameters
    );

    node
      .execute(outputPrevNode)
      .then((outputNode: INodeExecutionData) => {
        const next: Array<IConnection> = workflow.connections.filter(
          (c) => c.from === from
        );

        if (next.length !== 0) {
          //Execute next nodes
          next.forEach((c) => {
            resolve(executeNode(workflow, c.to, outputNode));
          });
        } else {
          resolve(outputNode);
        }
      })
      .catch((e) => {
        const r: INodeExecutionData = {
          endpointParameters: outputPrevNode.endpointParameters,
          executedNode: "failed",
          prevNodeData: [],
          json: {},
          error: "Failed to get node structure " + e,
        };

        reject(r);
      });
  });
}
