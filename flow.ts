import { IEndPoint, IWorkflowStructure } from "./utils/interfaces";

export const workflowExample: IWorkflowStructure = {
  name: "endpoint",
  active: true,
  nodes: [
    {
      id: 1,
      name: "Start node",
      type: "start",
      version: 1,
      parameters: {
        authentication: "start",
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
    {
      id: 2,
      name: "Mongo node",
      type: "mongodb",
      version: 1,
      parameters: {
        mongoUser: "mongouser",
        mongoUserPassword: "mongouser",
        cluster: "cluster0",
        databaseName: "collection1",
        action: "get",
        query: {},
        select: { _id: 0, ba: 0 },
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
    {
      id: 3,
      name: "Format node",
      type: "format",
      version: 1,
      parameters: {
        format: {
          superuser: "results",
        },
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
  ],
  connections: [
    {
      from: 1,
      to: 2,
      type: "normal",
    },
    {
      from: 2,
      to: 3,
      type: "normal",
    },
  ],
};

export const minExample: IWorkflowStructure = {
  name: "endpoint",
  active: true,
  nodes: [
    {
      id: 1,
      name: "Start node",
      type: "start",
      version: 1,
      parameters: {
        authentication: "start",
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
    {
      id: 2,
      name: "Log node",
      type: "log",
      version: 1,
      parameters: {},
      postition: {
        x: 100,
        y: 200,
      },
    },
  ],
  connections: [
    {
      from: 1,
      to: 2,
      type: "normal",
    },
  ],
};

const postWorkflow: IWorkflowStructure = {
  name: "Post workflow",
  active: false,
  nodes: [
    {
      id: 1,
      name: "Start node",
      type: "start",
      version: 1,
      parameters: {
        authentication: "start",
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
    {
      id: 2,
      name: "Mongo node",
      type: "mongodb",
      version: 1,
      parameters: {
        mongoUser: "mongouser",
        mongoUserPassword: "mongouser",
        cluster: "cluster0",
        databaseName: "collection1",
        action: "post",
      },
      postition: {
        x: 100,
        y: 200,
      },
    },
  ],
  connections: [
    {
      from: 1,
      to: 2,
      type: "normal",
    },
  ],
};

export const endpoints: Array<IEndPoint> = [
  {
    endpoint: "/",
    verb: "get",
    flow: workflowExample,
  },
  {
    endpoint: "/",
    verb: "post",
    flow: postWorkflow,
  },
  {
    endpoint: "/:id",
    verb: "get",
    flow: minExample,
  },
];
